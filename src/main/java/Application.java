import animals.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik kotik1 = new Kotik("Барсик", "мяу-мяу",7, 3);
        Kotik kotik2 = new Kotik("Кот");
        kotik2.setName("Васька");
        kotik2.setVoice("миу-миу");
        kotik2.setSatiety(5);
        kotik2.setWeight(4);
        kotik1.liveAnotherDay();
        kotik2.liveAnotherDay();
        compareVoice(kotik1, kotik2);
        System.out.println("создано " + Kotik.getCount() + " котика(ов)");
    }

    private static boolean compareVoice(Kotik k1, Kotik k2) {
        int ct = k1.getVoice().compareTo(k2.getVoice());
        System.out.println("эквивалент голосов котиков:" + " " + ct + " (разные)");
        return true;
    }
}
