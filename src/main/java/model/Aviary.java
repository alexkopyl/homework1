package model;

import animals.Animal;
import java.util.HashMap;
import java.lang.String;

public class Aviary<T extends Animal>{
    private Size size;
    private HashMap<String, Animal> aviaryMap = new HashMap<>();

    public <T> Aviary(Size size) {
        this.size = size;
    }

    public <T> void addAnimal(Animal animal) {
        //добавить животного
        if (animal.getSize() == size) {
            aviaryMap.put(animal.getName(), animal);
            System.out.println("Животное " + animal.getName() +
                    " добавлено в вольер");
        }
        else {
            throw new WrongSizeException();
        }

    }

    public Animal getAnimal(String name) {
        if (!aviaryMap.isEmpty()) {
            System.out.println(aviaryMap.containsKey(name));

        }
        else
        System.out.println("Вольер пустой");
        return null;
    }

    public boolean removeAnimal(String name) {
      if (aviaryMap.containsKey(name)) {
          System.out.println(aviaryMap.remove(name) + " Животное" +
                  " " + name + " удалено из вольера");
          return true;
      }
      else System.out.println("Данного животного нет в вольере");
        return false;
    }
}
