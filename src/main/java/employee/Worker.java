package employee;

import animals.*;
import food.Food;
import food.WrongFoodException;

public class Worker{
    public void feed(Animal animal, Food food) {
        try {
            animal.eat(food);
        }
        catch (WrongFoodException e) {
            e.printStackTrace();
        }


    }

    public void getVoice(Voice voice) {
        voice.getVoice();
    }

}
