
import animals.*;
import employee.Worker;
import food.Grass;
import food.Meat;
import model.Aviary;
import model.Size;


public class Zoo<carnivorousAviary, herbivoreAviary> {
    public static void main(String[] args) {
        Herbivore duck = new Duck("Утка");
        System.out.println(duck.hashCode());
        Herbivore elephant = new Elephant("Слон");
        Herbivore giraffe = new Giraffe("Жираф");
        Carnivorous fish = new Fish("Рыба");
        Carnivorous kotik1 = new Kotik("Кот");
        Carnivorous lion = new Lion("Лев");
        Grass grass = new Grass();
        Meat meat = new Meat();
        Worker worker = new Worker();
        worker.feed(duck, grass);
        worker.feed(elephant, meat);
        worker.feed(giraffe, grass);
        worker.feed(fish, grass);
        worker.feed(kotik1, meat);
        worker.feed(lion, grass);
        worker.getVoice((Voice) duck);
        worker.getVoice((Voice) elephant);
        worker.getVoice((Voice) kotik1);
        worker.getVoice((Voice) lion);
        for (int i = 0; i < createPond().length; i++);
        createPond()[0].swim();
        createPond()[1].swim();
    }

    private static Aviary<Carnivorous> carnivorousAviary = new Aviary<>(Size.MEDIUM);
    private static Aviary<Herbivore> herbivoreAviary = new Aviary<>(Size.LARGE);

    public static Swim[] createPond() {
        Swim[] swims = new Swim[2];
        swims[0] = new Duck("Утка");
        swims[1] = new Fish("Рыба");
        return swims;

    }

    public void fillCarnivorousAviary() {
        Carnivorous fish1 = new Fish("Рыба1");
        Carnivorous fish2 = new Lion("Рыба1");
        carnivorousAviary.addAnimal(fish1);
        carnivorousAviary.addAnimal(fish2);
    }

    public static void fillHerbivoreAviary() {
        Herbivore duck1 = new Duck("Утка1");
        Herbivore duck2 = new Duck("Утка2");
        herbivoreAviary.addAnimal(duck1);
        herbivoreAviary.addAnimal(duck2);
    }

    public static Carnivorous getCarnivorous(String name) {
        return (Carnivorous) carnivorousAviary.getAnimal(name);
    }

    public static Herbivore getHerbivore(String name) {
        return (Herbivore) herbivoreAviary.getAnimal(name);
    }
}
