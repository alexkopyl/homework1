package animals;

import food.Food;
import food.Grass;
import food.WrongFoodException;
import model.WrongSizeException;

public abstract class Carnivorous extends Animal  {
    public Carnivorous(String name) {
        super(name);
    }

    @Override
    public void eat(Food food) throws WrongFoodException {
        if (food instanceof Grass) {
            System.out.println("Еда не подходит животному");
            throw new WrongFoodException();
        }
        else
            setSatiety(food.getEnergy());
            System.out.println("Животное поело");
    }
}

