package animals;

import model.Size;

public class Duck extends Herbivore implements Fly, Swim, Voice, Run{
    public Duck(String name) {
        super(name);
    }

    @Override
    public void fly() {
        System.out.println("Животное умеет летать");
    }

    @Override
    public boolean swim() {
        System.out.println("Животное умеет плавать");
        return false;
    }

    @Override
    public String getVoice() {
        System.out.println("голосит " + "Кря-Кря");
        return "Кря-Кря";
    }

    @Override
    public void run() {
        System.out.println("Животное умеет бегать");
    }

    @Override
    public Size getSize() {
        return Size.SMALL;
    }
}
