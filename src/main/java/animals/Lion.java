package animals;

import model.Size;

public class Lion extends Carnivorous implements Voice, Run, Swim{
    public Lion(String name) {
        super(name);
    }

    @Override
    public void run() {
        System.out.println("Животное  умеет бегать");
    }

    @Override
    public boolean swim() {
        System.out.println("Животное  умеет плавать");
        return false;
    }

    @Override
    public String getVoice() {
        System.out.println("голосит " + "Арр-Арр");
        return "Арр-Арр";
    }

    @Override
    public Size getSize() {

        return Size.MEDIUM;
    }
}
