package animals;

import model.Size;

public class Kotik extends Carnivorous implements Voice, Run{
    private String name;
    private String voice = "";
    private int satiety;
    private int weight;
    private static int count = 0;
    final static private int METHODS = 5; //типы поведения

    public String getVoice() {
        System.out.println("голосит " + "Мяу-Мяу");
        return "Мяу-Мяу";
    }

    public int getSatiety(int i) {
        return satiety;
    }

    public int getWeight() {
        return weight;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }

    @Override
    public Size getSize() {

        return Size.SMALL;
    }

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Kotik(String name, String voice, int satiety, int weight) {
        super(name);

        this.name = name;
        this.voice = voice;
        this.satiety = satiety;
        this.weight = weight;
        ++count;
    }

    public Kotik(String name) {
        super(name);

        ++count;
    }


    public static int getCount() {
        return count;
    }

    public boolean play() {
            if (satiety > 0) {
                satiety--;
                return true;
            }else
        return false;
    }

    public boolean sleep() {
            if (satiety > 0) {
                satiety--;
                return true;
            }else
        return false;
    }

    public boolean wash() {
            if (satiety > 0) {
                satiety--;
                return true;
            }else
        return false;
    }
    public boolean walk() {
            if (satiety > 0) {
                satiety--;
                return true;
            }else
        return false;
    }

    public boolean hunt() {
            if (satiety > 0) {
                satiety--;
                return true;
            }else
        return false;
    }

    public void eat(int satietyCount) {
        satiety += satietyCount;
    }

    public void eat(int satietyCount, String eatName) {
        satiety += satietyCount;
    }

    public void eat() {
        eat(1,"whiskas" );
    }

    public String [] liveAnotherDay() {
        System.out.println("Имя котика: " + getName());
        System.out.println("Вес котика: " + getWeight() + " кг");
        String [] lAD = new String[24];
        for (int i = 0; i < lAD.length; i++) {
            int methodNum = (int) (Math.random() * METHODS) + 1;
            switch (methodNum) {
                case 1:
                    if (play()) {
                        lAD[i] = i + " - " + "Играл";
                    }
                    break;
                case 2:
                    if (sleep()) {
                        lAD[i] = i + " - " + "Спал";
                    }
                    break;
                case 3:
                    if (wash()) {
                        lAD[i] = i + " - " + "Умывался";
                    }
                    break;
                case 4:
                    if (walk()) {
                        lAD[i] = i + " - " + "Гулял";
                    }
                    break;
                case 5:
                    if (hunt()) {
                        lAD[i] = i + " - " + "Охотился";
                    }
                    break;
            }
            if (satiety <= 0) {
                eat(5);
            }
        }
        for (String str: lAD
             ) {
            System.out.println(str);
        }
        System.out.println();
        return lAD;
    }

    @Override
    public void run() {
        System.out.println("Животное " + getName() + " умеет бегать");
    }
}