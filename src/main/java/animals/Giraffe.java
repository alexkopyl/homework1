package animals;

import model.Size;

public class Giraffe extends Herbivore implements Run{
    public Giraffe(String name) {
        super(name);
    }

    @Override
    public void run() {
        System.out.println("Животное  умеет бегать");
    }

    @Override
    public Size getSize() {

        return Size.LARGE;
    }
}
