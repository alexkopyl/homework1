package animals;

import model.Size;

public class Fish extends Carnivorous implements Swim{
    public Fish(String name) {
        super(name);
    }

    @Override
    public boolean swim() {
        System.out.println("Животное умеет плавать");
        return false;
    }

    @Override
    public Size getSize() {

        return Size.MEDIUM;
    }
}
