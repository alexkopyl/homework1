package animals;

import food.Food;
import food.WrongFoodException;
import model.Size;

import java.util.UUID;

public abstract class Animal {

    private int satiety = 0;
    private String name;


    public Animal(String name) {
        this.name = name;
    }

    public abstract void eat(Food food) throws WrongFoodException;

    public abstract Size getSize();

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    public int getSatiety() {
        return satiety;
    }

    public String getName() {
        return name;
    }
}
