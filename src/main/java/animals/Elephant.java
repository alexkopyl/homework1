package animals;

import model.Size;

public class Elephant extends Herbivore implements Run, Voice{
    public Elephant(String name) {
        super(name);
    }

    @Override
    public void run() {
        System.out.println("Животное  умеет бегать");
    }

    @Override
    public String getVoice() {
        System.out.println("голосит " + "Иуу-Уии");
        return "Иуу-Иуу";
    }

    @Override
    public Size getSize() {

        return Size.LARGE;
    }
}
