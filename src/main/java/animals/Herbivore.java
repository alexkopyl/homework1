package animals;

import food.Food;
import food.Meat;
import food.WrongFoodException;
import model.WrongSizeException;

public abstract class Herbivore extends Animal {
    public Herbivore(String name) {
        super(name);
    }

    @Override
    public void eat(Food food) throws WrongFoodException {
        if (food instanceof Meat) {
            System.out.println("Еда не подходит животному");
            throw new WrongSizeException();
        }
        else
            setSatiety(food.getEnergy());
            System.out.println("Животное поело");
    }
}

